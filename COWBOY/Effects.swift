//
//  ObjectEffects.swift
//  COWBOY
//
//  Created by Илья Руденцов on 13.05.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import SpriteKit

class Effects: NSObject{
    
    func returnInHand(body:SKSpriteNode){
        var nodeBody = body
        var hand = nodeBody.childNodeWithName("hand") as! SKSpriteNode
        
        var oldPositionX = hand.position.x
        var a1 = SKAction.moveToX(hand.position.x - 3, duration: 0.05)
        var a2 = SKAction.moveToX(oldPositionX, duration: 0.05)
        
        hand.runAction(SKAction.sequence([a1,a2]), completion: { () -> Void in
            hand.position = CGPointMake(oldPositionX, hand.position.y)
        })
    }
    
}

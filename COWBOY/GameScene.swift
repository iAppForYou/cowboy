//
//  GameScene.swift
//  COWBOY
//
//  Created by Илья Руденцов on 11.05.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import SpriteKit
import Darwin
var isCreated: Bool = false
// The root node of your game world. Attach game entities
// (player, enemies, &c.) to here.
let world = SKNode()
// The root node of our UI. Attach control buttons & state
// indicators here.
let overlay = SKNode()
// The camera. Move this node to change what parts of the world are visible.
let camera = SKNode()

var joystick = Joystick()

let cowboyCategory: UInt32 = 0x1 << 1
class GameScene: SKScene, SKPhysicsContactDelegate {

    var panel = SKSpriteNode()
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = UIColor.whiteColor()
        self.physicsWorld.contactDelegate = self
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: CGRectMake(0, 0, width/2, height/2))
        
        createControlPanel()
        
        var playerCowboy = Factory().Cowboy("body", physicsBody: true, affectedByGravity: false, dynamic: false, name: "playerCowboy")
        playerCowboy.position  = CGPointMake(width/11, panel.size.height + playerCowboy.size.height/2+1)
        self.addChild(playerCowboy)
        
        var enemyCowboy = Factory().Cowboy("body", physicsBody: true, affectedByGravity: false, dynamic: false, name: "enemyCowboy")
        enemyCowboy.position  = CGPointMake(width/2 - width/11, panel.size.height + enemyCowboy.size.height/2+1)
        enemyCowboy.xScale = -1.0
        self.addChild(enemyCowboy)
        
        var sight = Factory().sight("playerSight", size: 10, color: UIColor.redColor())
        sight.position = CGPointMake(width / 4, height / 4)
        println(sight.position)
        self.addChild(sight)
        
        joystick = Joystick()
        joystick.position = CGPointMake(80,55)
        self.addChild(joystick)
        
    }


    func createControlPanel(){
        panel = SKSpriteNode(color: MyFuncs().rgb(166, g: 226, b: 163), size: CGSizeMake(width, 110))
        panel.position=CGPointMake(0, panel.size.height/2)
        panel.name="panel"
        panel.physicsBody=SKPhysicsBody(rectangleOfSize: panel.size)
        panel.physicsBody?.dynamic=false
        panel.physicsBody?.affectedByGravity=false
        self.addChild(panel)
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            //Control().handMove1(self.childNodeWithName("playerCowboy") as! SKSpriteNode, location: location)
            Control().shot(self.childNodeWithName("playerCowboy") as! SKSpriteNode, location: location)
            
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            
            //Control().handMove1(self.childNodeWithName("playerCowboy") as! SKSpriteNode, location: location)
            
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        Control().sightMove(self.childNodeWithName("playerCowboy") as! SKSpriteNode)
        
    }
}

//
//  ObjectControl.swift
//  COWBOY
//
//  Created by Илья Руденцов on 13.05.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import SpriteKit
 // PI / 180

class Control: NSObject{

    func handMove1(body:SKSpriteNode, location:CGPoint){
       
        var nodeBody = body
        if(location.x < height/2){
        
        var hand = nodeBody.childNodeWithName("hand")
            hand?.zRotation = location.y / 180 * CGFloat(M_PI)
        }
        
    }
    
    func handMove2(body:SKSpriteNode, location:CGPoint){
        
        var nodeBody = body
        
        var hand = nodeBody.childNodeWithName("hand") as! SKSpriteNode
        var positionInScene = nodeBody.parent!.convertPoint(hand.position, fromNode: body)
        
        var d = location.x - positionInScene.x
        var z = location.y - positionInScene.y
        
        var c = sqrt( pow(d, 2) + pow(z, 2) )
        
        var x = z / d
        
        //hand.size=CGSizeMake(20, c)
        //hand.zPosition=10
        //hand.anchorPoint=CGPointMake(0.0, 0.0)
        
        //println(x)
        hand.zRotation = x + 90 * CGFloat(M_PI) / 180
    }
    
    func sightMove(body:SKSpriteNode){
        var nodeBody = body
        var sight = nodeBody.parent!.childNodeWithName("playerSight") as! SKSpriteNode
        if joystick.velocity.x != 0 || joystick.velocity.y != 0 {
            sight.position = CGPointMake(sight.position.x + 0.1 * joystick.velocity.x, sight.position.y + 0.1 * joystick.velocity.y)
            
            var hand = nodeBody.childNodeWithName("hand") as! SKSpriteNode
            var positionInScene = nodeBody.parent!.convertPoint(hand.position, fromNode: body)
            
            var d = sight.position.x - positionInScene.x
            var z = sight.position.y - sight.size.height/2 - positionInScene.y
            var c = sqrt( pow(d, 2) + pow(z, 2) )
            var x = z / d
            
            hand.zRotation = x + 90 * CGFloat(M_PI) / 180
        }
    }
    
    func shot(body:SKSpriteNode, location: CGPoint){
        if(location.x > height/2){
            Factory().Bullet(body, location: CGPoint(), affectedByGravity:false, dynamic:true, speed:3.0, size:4)
            Effects().returnInHand(body)
        }
        
    }
    
}
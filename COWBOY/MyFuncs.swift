//
//  MyFuncs.swift
//  COWBOY
//
//  Created by Илья Руденцов on 11.05.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//
//
import Foundation
import UIKit
import SpriteKit

class MyFuncs: NSObject {
    
    func rgb(r:CGFloat, g:CGFloat, b:CGFloat)->UIColor{
        var color = UIColor(red: (r/255), green: (g/255), blue: (b/255), alpha: 1.0)
        return color
    }
    
    
}

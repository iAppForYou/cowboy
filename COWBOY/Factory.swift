//
//  ObjectFactory.swift
//  COWBOY
//
//  Created by Илья Руденцов on 13.05.15.
//  Copyright (c) 2015 Илья Руденцов. All rights reserved.
//

import SpriteKit

class Factory: NSObject{
    
    func Cowboy(imageName:String, physicsBody:Bool, affectedByGravity:Bool, dynamic:Bool, name:String)->SKSpriteNode{
        
        if(imageName == ""){imageName == "body.png"}

        
        var spriteBody = SKSpriteNode(imageNamed: imageName)
        spriteBody.name=name
if(physicsBody == true){ spriteBody.physicsBody = SKPhysicsBody(texture: spriteBody.texture, size: spriteBody.size)}
        spriteBody.physicsBody?.affectedByGravity = affectedByGravity
        spriteBody.physicsBody?.dynamic = dynamic
        
        var spriteHat = SKSpriteNode(imageNamed: "hat")
        spriteHat.physicsBody = SKPhysicsBody(texture: spriteHat.texture, size: spriteHat.size)
        spriteHat.physicsBody?.affectedByGravity = true
        spriteHat.physicsBody?.dynamic = true
        spriteHat.position = CGPointMake(-10, 57)
        spriteHat.physicsBody?.mass = 0.001
        spriteBody.addChild(spriteHat)
        
        var spriteHand = SKSpriteNode(imageNamed: "hand")
        spriteHand.position = CGPointMake(-18, 8)
        spriteHand.name="hand"
        spriteHand.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        
        spriteBody.addChild(spriteHand)
        
        var spriteBulletMark = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(1, 1))
        spriteBulletMark.name = "bulletMark"
        spriteBulletMark.position=CGPointMake(spriteHand.size.width/4, -spriteHand.size.height-spriteBulletMark.size.height/2 - 10)
        spriteHand.addChild(spriteBulletMark)
        
        var spriteBulletMark2 = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(1, 1))
        spriteBulletMark2.name = "bulletMark2"
        spriteBulletMark2.position=CGPointMake(spriteHand.size.width/4, -spriteHand.size.height-spriteBulletMark.size.height/2 - 20)
        spriteHand.addChild(spriteBulletMark2)
        
        
        return spriteBody
    }
    
    func Bullet(body:SKSpriteNode, location:CGPoint, affectedByGravity:Bool, dynamic:Bool, speed:CGFloat, size:CGFloat) {
        
        var nodeBody = body
        var hand = nodeBody.childNodeWithName("hand") as! SKSpriteNode
        
        var bullet = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(size, size))
        
        bullet.physicsBody = SKPhysicsBody(rectangleOfSize: bullet.size)
        bullet.physicsBody?.affectedByGravity = affectedByGravity
        bullet.physicsBody?.dynamic=dynamic
        
        var bulletMark = hand.childNodeWithName("bulletMark") as! SKSpriteNode
        var bulletMark2 = hand.childNodeWithName("bulletMark2") as! SKSpriteNode
        
        var positionInScene = nodeBody.parent!.convertPoint(bulletMark.position, fromNode: hand)
        var positionInScene2 = nodeBody.parent!.convertPoint(bulletMark2.position, fromNode: hand)
        bullet.position = positionInScene
        
        nodeBody.parent!.addChild(bullet)
        
        if location == CGPoint(){
        bullet.physicsBody?.applyForce(CGVectorMake((positionInScene2.x - positionInScene.x) * speed, (positionInScene2.y - positionInScene.y) * speed))
        }else{
        bullet.physicsBody?.applyForce(CGVectorMake((location.x - positionInScene.x) * speed, (location.y - positionInScene.y) * speed))
        }
        
        bullet.runAction(SKAction.sequence([SKAction.waitForDuration(5.0), SKAction.fadeOutWithDuration(0.2),SKAction.removeFromParent()]))
        
        //return SKSpriteNode()
    }
    
    func sight(name:String, size:CGFloat, color:UIColor) -> SKSpriteNode{
        
        var sight = SKSpriteNode(color: color, size: CGSizeMake(size, size))
        
        sight.name = name
        sight.zPosition = 4
        
        return sight
    }


}
